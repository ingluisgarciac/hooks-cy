import { useState, useEffect } from 'react';

const Seconds =()=> {
  /*
  state = { seconds: 0 };

  componentDidMount() {
    this.intervalId = setInterval(() => {
        this.setState(state => ({ seconds: state.seconds + 1 }));
       }, 1000)  
  }

  componentWillMount() {
    clearInterval(this.intervalId);
  }
  
  
    //const { seconds } = this.state;

    //useEffects:  componentDidMount / componentDidUpdate / ¿componentWillUnmount?

    state => ({ seconds: state.seconds + 1 })
*/
const [seconds, setSeconds] = useState(0);

    useEffect(()=>{
      const intervalId = setInterval(() => {
        setSeconds(seconds => seconds+1);
       }, 1000);
       return ()=> {
        clearInterval(intervalId);
       }  
    }, [seconds])

    
    return seconds;
  
}

export default Seconds;